var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var fileuploadApi = require('./api/fileupload');
var downloadApi = require('./api/download');
var session = require('express-session');
var FileStore = require('session-file-store')(session);

const formidableMiddleware = require('express-formidable');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(formidableMiddleware());

// Use the session middleware
app.use(session({ secret: 'cloudfish',
                  resave: true,
                  saveUninitialized: true, 
                  cookie: { maxAge: 60000 },
                  store: new FileStore()
                }));


app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/fileupload', formidableMiddleware(), fileuploadApi);
app.use('/download', downloadApi );

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
