var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var imgFile = '';
  if(req.query.f){
    imgFile = req.query.f;
  }
  res.render('index', { title: 'Express','imgFile':imgFile });
});

module.exports = router;
