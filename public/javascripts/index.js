$('#openImageUploadButton').click(function(){
     $('#imageUpload').trigger('click');
     return false;
});

$('#imageUpload').change(function(){
    var imgUpload =$('#imageUpload')[0];
     if(imgUpload){
         if(imgUpload.files.length > 0)
         {
            console.log(imgUpload.files[0].name);
            $('#filename').val(imgUpload.files[0].name);
            $('#submitButton').trigger('click');
         }
     }
});

$('#downloadImage').click(function(){
    uploadCanvasToServer()
    .then(function(fileName){
        loadToCanvas('./download/?f=' + fileName);
        downloadFile('./download/d/?f=' + fileName);
    })
    .catch(function(ex){
        console.error(ex.xhr);
        console.error(ex.data);
    });
});

setTimeout(checkSize, 1000);

function checkSize(){
    console.log($('#mainImage').width());
    console.log($('#mainImage').height());
    setTimeout(checkSize, 1000);
    if($('#mainImage').width()){
        $('#mainImage')[0].style.width='';
        $('#mainImage')[0].style.height='';
    }
}

function loadToCanvas(uri){
    Caman("#mainImage", uri);
    $('.imageActionButton').show();
}


function downloadFile(uri){
    window.location.href=uri;
}

function uploadCanvasToServer(){
    return new Promise((resolve, reject)=>{
        var formData = new FormData();
        var dataURI = $('#mainImage')[0].toDataURL();
        var blob = dataURItoBlob(dataURI);
        formData.append('image', blob);
        formData.append('uploadType', 'forDownload')
        $.ajax({
            url:'/fileupload',
            type:'POST',
            data:formData,
            processData: false,
            contentType: false
        }).done(function(data){
            resolve(data)
        }).fail(function(xhr, data){
            reject({xhr:xhr, data: data});
        });
    });
    
}

function dataURItoBlob(dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var dw = new DataView(ab);
    for(var i = 0; i < byteString.length; i++) {
        dw.setUint8(i, byteString.charCodeAt(i));
    }

    // write the ArrayBuffer to a blob, and you're done
    return new Blob([ab], {type: mimeString});
}

