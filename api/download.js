var express = require('express');
var router = express.Router();
var fs = require('fs');
var os = require('os');
const path = require('path');

router.get('/', function(req, res, next){
  if(req.query.f){
    var filePath = os.tmpdir() + path.sep + req.query.f;
    if(fs.existsSync(filePath)){
      var img = fs.readFileSync(filePath);
      res.writeHead(200, {'Content-Type': 'image/binary','Content-Disposition': 'attachment;filename=' + req.query.f });
      res.end(img, 'binary');
    }
    else{
      res.writeHead(404);
      res.end();
    }
  }
  else{
    res.writeHead(404);
    res.end();
  }
});

router.get('/d', function(req, res, next){
  if(req.query.f){
    var filePath = os.tmpdir() + path.sep + req.query.f;
    if(fs.existsSync(filePath)){
      var img = fs.readFileSync(filePath);
      var targetFileName = req.query.f;
      if(req.session.localFileName){
        targetFileName = req.session.localFileName;
      }
      res.writeHead(200, {'Content-Type': 'application/force-download','Content-Disposition': 'attachment;filename=' + targetFileName });
      res.end(img, 'binary');
    }
    else{
      res.writeHead(404);
      res.end();
    }
  }
  else{
    res.writeHead(404);
    res.end();
  }
});


module.exports = router;
