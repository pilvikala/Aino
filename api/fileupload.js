var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next){
  res.writeHead(405);
  res.end();
});

router.post('/', function(req, res, next) {
  if(req.files && req.files['image']){
    if(req.fields.uploadType == 'forDownload'){
      uploadForDownload(req, res);
    }
    else{
      uploadNewFile(req, res);
    }
  }
  else{
    res.writeHead(400, 'no file uploaded');
  }
});

module.exports = router;

function uploadNewFile(req, res){
  var filename = req.files["image"].path.replace(/^.*[\\\/]/, '');
  req.session.localFileName = req.fields["filename"];
  req.session.save();
  res.writeHead(301,
    {Location: '/?f='+filename}
  );
  res.end();
}

function uploadForDownload(req, res){
  var filename = req.files["image"].path.replace(/^.*[\\\/]/, '');
  res.writeHead(200);
  res.write(filename);
  res.end();
}
